# Home Excercise for Lumin Skin

This is a basic Pub/Sub system implemented on Express.js.

## Installation

You can install using npm.

Go to pubsub1 folder and run.

```bash
npm install
```

## Start

After installation you can run:

```js
node main.js
```

## Usage

By default server run in 8000 port. So you can work in http://localhost:8000/.

System is working with a mocked storage layer. It could be implementes with any DB system if necessary.  

You have access to the next endpoints:

- **(get) /** --> Root will show all the published messages in system.
- **(post) /event** -->  This will work as a mock of a subscriptor who can be subscribed to any topic. It just print the message content when a new message has been published in some topic he is subscribed.
- **(post) /subscribe/{any topic}**  --> This is used for subscribe new subscriptors to any topic. Topic could be exists or not, if not it will be created. You need to send the subscriptor data as follow `{"url": "http://localhost:8000/event"}` and this subscriptor will be notified about new messages in this topic.
- **(post) /publish/{any topic}** -->  This is used for publish a new message in a specific topic. You need to send the message you want to publish as folows: `{"message": "This is an amazing message to publish!"}`


You can use cURL to test as follows:

- For subscribe **http://localhost:8000/event** to the topic **topic1** you can use:
```bash
curl -X POST -H "Content-Type: application/json" -d '{"url": "http://localhost:8000/event"}' http://localhost:8000/subscribe/topic1
```

- For publish **A super message** to the topic **topic1** you can use:
```bash
curl -X POST -H "Content-Type: application/json" -d '{"message": "A super message"}' http://localhost:8000/publish/topic1
```

- After you have been subscribed **http://localhost:8000/event** to the **topic1** you will see in your console a notification about any message you publish to this topic.