const express = require('express');

const pubsub = require('./pubsub/pubsub');

const PORT = 8000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get('/', (req, res) => {
    let messages = pubsub.getSentMessages();
    res.json(messages);
})

app.post('/subscribe/:topic', (req, res) => {
    let url = req.body.url;
    let topic = req.params.topic;
    let subscription = pubsub.subscribe(url, topic);
    
    res.json({
        message: 'Subscription created!',
        url,
        subscription
    });
})

app.post('/publish/:topic', (req, res) => {
    let message = req.body.message;
    let topic = req.params.topic;
    let data = pubsub.publish(message, topic);
    res.json({
        message: 'Message has been published',
        data
    })
})

app.post('/event', (req, res) => {
    console.log(`There is a new message for ${req.url}:`, req.body.message);
})

app.listen(PORT, () => {
    console.log(`Listening on ${PORT} port.`);
});

