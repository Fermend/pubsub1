const subscribersStore = require('./store/subscribers.store');
const messagesStore = require('./store/messages.store');
const axios = require('axios');

let publish = (message, topic) => {
    messagesStore.store(message, topic);
    forward(message, topic);
    return {message, topic};
}

let subscribe = (url, topic) => {
    let data = subscribersStore.storeSubscriberByTopic(url, topic);
    console.log(`${url} has been subscribed to topic ${topic}`);
    return data;
}

let forward = (message, topic) => {
    let subscribersByTopic = subscribersStore.listSubscribersByTopic(topic);
    subscribersByTopic.forEach( subscriberUrl => {
        axios.post(subscriberUrl, {message});
    });
}

let getSentMessages = () => {
    let messages = messagesStore.list();
    return {
        messages
    }
}

module.exports = {
    publish,
    subscribe,
    forward,
    getSentMessages
}
