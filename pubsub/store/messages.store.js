let messages = [];

const list = () => {
    return messages;
}

const store = (message, topic) => {
    let newMessage = {
        message,
        topic
    }
    messages = [...messages, newMessage]
    return newMessage;
}

module.exports = {
    list,
    store
}