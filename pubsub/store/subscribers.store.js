
const subscribers = {};

let listSubscribersByTopic = (topic) => {
    let subscribersByTopic = subscribers[topic] || [];
    return subscribersByTopic;
}

let storeSubscriberByTopic = (subscriptor, topic) => {
    if(!subscribers[topic]){
        subscribers[topic] = [];
    }
    subscribers[topic] = [...subscribers[topic], subscriptor];
    return {
        subscriptor,
        topic
    };
}

module.exports = {
    listSubscribersByTopic,
    storeSubscriberByTopic
}